#include "template_method.hpp"

using namespace std;

int main()
{
	AbstractClass* c = new ConcreteClassA;
	c->template_method();

	delete c;

    cout << "\n\n";

	c = NULL;

	c = new ConcreteClassB();
	c->template_method();

	delete c;
}
