#include <iostream>

class Turnstile
{
public:
    virtual void Lock() { std::cout << "Lock" << std::endl; }
    virtual void Unlock() { std::cout << "Unlock" << std::endl; };
    virtual void Thankyou() { std::cout << "Thank You" << std::endl; };
    virtual void Alarm() { std::cout << "Alarm" << std::endl; };
    virtual ~Turnstile() {}
};

class TurnstileFSM;
class LockedState;
class UnlockedState;

class TurnstileState
{
public:
    virtual TurnstileState* Coin(Turnstile&) = 0;
    virtual TurnstileState* Pass(Turnstile&) = 0;
	virtual ~TurnstileState() {}
};

class UnlockedState : public TurnstileState
{
public:
    TurnstileState* Coin(Turnstile& t);
    TurnstileState* Pass(Turnstile& t);
};

class LockedState : public TurnstileState
{
public:
    TurnstileState* Coin(Turnstile& t);
    TurnstileState* Pass(Turnstile& t);
};

class TurnstileFSM
{
    Turnstile turnstile_;
public:
	TurnstileFSM() : itsState(&lockedState)
	{
	}

    void Coin() { itsState = itsState->Coin(turnstile_); }
    void Pass() { itsState = itsState->Pass(turnstile_); }
	
	static LockedState lockedState;
	static UnlockedState unlockedState;

private:
	TurnstileState* itsState;
};

LockedState TurnstileFSM::lockedState;
UnlockedState TurnstileFSM::unlockedState;

TurnstileState* UnlockedState::Coin(Turnstile& t)
{
    t.Thankyou();
	return this;
}

TurnstileState* UnlockedState::Pass(Turnstile& t)
{
    t.Lock();
	return &TurnstileFSM::lockedState;
}

TurnstileState* LockedState::Coin(Turnstile& t)
{
    t.Unlock();
	return &TurnstileFSM::unlockedState;
}

TurnstileState* LockedState::Pass(Turnstile& t)
{
    t.Alarm();
	return this;
}

int main()
{
	TurnstileFSM turnstile;

	turnstile.Coin();
	turnstile.Pass();
	turnstile.Pass();
	turnstile.Coin();
	turnstile.Coin();
}
