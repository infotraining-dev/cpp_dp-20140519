#include "document.hpp"
#include "command.hpp"
#include "application.hpp"

#include <stack>
#include <string>

int main()
{
	Document* doc = new Document("Wzorzec projektowy: Command");

	// obiekty polecen
	Command* cmd_paste = new PasteCmd(doc);
	Command* cmd_undo = new UndoCmd(doc);
	Command* cmd_print = new PrintCmd(doc);
	Command* cmd_toupper = new ToUpperCmd(doc);
    Command* cmd_copy = new CopyCmd(doc);
    Command* cmd_tolower = new ToLowerCmd(doc);
    Command* cmd_addtext = new AddTextCmd(doc);
    // TODO: CopyCmd
	// TODO: ToLowerCmd
	// TODO: AddTextCmd

	// przyciski
	MenuItem* menu_paste = new MenuItem("Paste", cmd_paste);
	MenuItem* menu_toupper = new MenuItem("ToUpper", cmd_toupper);
	MenuItem* menu_undo = new MenuItem("Undo", cmd_undo);
	MenuItem* menu_print = new MenuItem("Print", cmd_print);
    MenuItem* menu_copy = new MenuItem("Copy", cmd_copy);
    MenuItem* menu_tolower = new MenuItem("ToLower", cmd_tolower);
    MenuItem* menu_addtext = new MenuItem("AddText", cmd_addtext);
    // TODO: menu_copy
	// TODO: menu_tolower
	// TODO: menu_add

	// aplikacja
	Application app(doc);
	app.add_menu("Paste", menu_paste);
	app.add_menu("ToUpper", menu_toupper);
	app.add_menu("Print", menu_print);
	app.add_menu("Undo", menu_undo);
    app.add_menu("Copy", menu_copy);
    app.add_menu("AddText", menu_addtext);
    app.add_menu("ToLower", menu_tolower);

	// TODO: menu_copy
	// TODO: menu_tolower
	// TODO: menu_add

	std::string cmd;
	do
	{
		std::cout << "Podaj komende: ";
		std::cin >> cmd;
		app.execute_action(cmd);
	} while (cmd != std::string("END"));

	std::cout << "Koniec pracy..." << std::endl;

	delete cmd_paste;
	delete cmd_undo;
	delete cmd_print;
	delete cmd_toupper;

	delete menu_paste;
	delete menu_toupper;
	delete menu_undo;
	delete menu_print;

	delete doc;
}

