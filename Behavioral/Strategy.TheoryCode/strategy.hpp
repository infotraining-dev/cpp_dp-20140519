#ifndef STRATEGY_HPP_
#define STRATEGY_HPP_

#include <iostream>

// "Strategy"
class Strategy
{	
public:
	virtual void algorithm_interface() = 0;
	virtual ~Strategy() {}
};

// "ConcreteStrategyA"
class ConcreteStrategyA : public Strategy
{
public:
	virtual void algorithm_interface()
	{
		std::cout << "Called ConcreteStrategyA.algorithm_interface()\n";
	}
};

// "ConcreteStrategyB"
class ConcreteStrategyB : public Strategy
{
public:
	virtual void algorithm_interface()
	{
		std::cout << "Called ConcreteStrategyB.algorithm_interface()\n";
	}
};

// "ConcreteStrategyC"
class ConcreteStrategyC : public Strategy
{
public:
	virtual void algorithm_interface()
	{
		std::cout << "Called ConcreteStrategyC.algorithm_interface()\n";
	}
};

// "Context" 
class Context
{
	Strategy* strategy_;

	Context(const Context&);
	Context& operator=(const Context&);
    // Constructor
public:
	Context(Strategy* strategy) : strategy_(strategy)
    {
    }
	
	void reset_strategy(Strategy* new_strategy)
	{
		if (strategy_ != new_strategy)
			delete strategy_;
		
		strategy_ = new_strategy;
	}

    void context_interface()
    {
    	strategy_->algorithm_interface();
    }
    
    ~Context()
    {
    	delete strategy_;
    }
};

#endif /*STRATEGY_HPP_*/
