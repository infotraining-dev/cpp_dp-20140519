#ifndef EMPLOYEE_HPP_
#define EMPLOYEE_HPP_

#include <iostream>
#include <string>
#include <memory>

class HRInfo;

//typedef std::auto_ptr<HRInfo> PtrHRInfo;
typedef std::unique_ptr<HRInfo> PtrHRInfo;

class Employee
{
protected:
	std::string name_;
public:
	Employee(const std::string& name);

	virtual void description() const = 0;

    virtual PtrHRInfo create_hrinfo() const;

	virtual ~Employee();
};

class Salary : public Employee
{
public:
	Salary(const std::string& name);

	virtual void description() const;
};

class Hourly : public Employee
{
public:
	Hourly(const std::string& name);

	virtual void description() const;
};

class Temp : public Employee
{
public:
	Temp(const std::string& name);

	virtual void description() const;

    virtual PtrHRInfo create_hrinfo() const;
};

#endif /* EMPLOYEE_HPP_ */
