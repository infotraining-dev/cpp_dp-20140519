#include <iostream>
#include <cassert>
#include <typeinfo>

using namespace std;

class Engine
{
public:
    virtual ~Engine() {}
    virtual void start() = 0;
    virtual void stop() = 0;

    Engine* clone() const
    {
        Engine* cloned_engine = do_clone();
        assert(typeid(*cloned_engine) == typeid(*this));

        return cloned_engine;
    }

protected:
    virtual Engine* do_clone() const = 0;
};

class Diesel : public Engine
{
public:
    virtual void start()
    {
        cout << "Diesel starts\n";
    }

    virtual void stop()
    {
        cout << "Diesel stops\n";
    }
protected:
    virtual Diesel* do_clone() const
    {
        return new Diesel(*this);
    }
};

class TDI : public Diesel
{
public:
    virtual void start()
    {
        cout << "TDI starts\n";
    }

    virtual void stop()
    {
        cout << "TDI stops\n";
    }
protected:
    TDI* do_clone() const
    {
        return new TDI(*this);
    }
};

// CRTP
template <typename EngineType>
class CloneableEngine : public Engine
{
protected:
    Engine* do_clone() const
    {
        return new EngineType(*static_cast<const EngineType*>(this));
    }
};

class Hybrid : public CloneableEngine<Hybrid>
{
public:
    virtual void start()
    {
        cout << "Hybrid starts\n";
    }

    virtual void stop()
    {
        cout << "Hybrid stops\n";
    }
};

class Car
{
    Engine* engine_;
public:
    Car(Engine* engine) : engine_(engine)
    {}

    Car(const Car& source) : engine_(source.engine_->clone())
    {
    }

    ~Car() { delete engine_; }

    void drive(int km)
    {
        engine_->start();
        cout << "Driving " << km << " kms\n";
        engine_->stop();
    }
};

int main()
{
    Car c1(new TDI());

    c1.drive(100);

    cout << "\n";

    Car copy_of_c1 = c1;

    copy_of_c1.drive(400);
}

