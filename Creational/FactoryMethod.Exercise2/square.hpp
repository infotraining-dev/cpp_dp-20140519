/*
 * square.hpp
 *
 *  Created on: 04-02-2013
 *      Author: Krystian
 */

#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "shape.hpp"

// TODO: Doda� klase Square
namespace Drawing
{
    class Square : public ShapeBase
    {
        int size_;
    public:
        Square(int x = 0, int y = 0, int size = 0)
            : ShapeBase(x, y), size_(size)
        {
        }

        void draw() const
        {
            std::cout << "Drawing a square at: " << point()
                      << " with size: " << size_ << std::endl;
        }

        void read(std::istream &in)
        {
            Point pt;
            int size;

            in >> pt >> size;

            set_point(pt);
            size_ = size;
        }

        void write(std::ostream &out)
        {
            out << "Point " << point() << " " << size_ << std::endl;
        }
    };
}


#endif /* SQUARE_HPP_ */
