#include "square.hpp"
#include "shape_factory.hpp"

namespace
{

    using namespace Drawing;

    Shape* create_square()
    {
        return new Square();
    }

    bool is_registered = ShapeFactory::instance()
                            .register_creator("Square", &create_square);
}
