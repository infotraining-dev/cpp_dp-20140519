#ifndef CLONE_FACTORY_HPP
#define CLONE_FACTORY_HPP

#include <string>
#include <map>
#include <stdexcept>
#include <boost/shared_ptr.hpp>
#include "shape.hpp"
#include "singleton_holder.hpp"

namespace Drawing
{

class Factory
{
public:
	typedef std::map<std::string, boost::shared_ptr<Shape> > PrototypesMap;

private:
	PrototypesMap prototypes_;
public:
	Shape* create(const std::string& id_type)
	{
		if (prototypes_.find(id_type) != prototypes_.end())
		{
			return prototypes_[id_type]->clone();
		}

		throw std::runtime_error("Bad shape type");
	}

	bool register_shape(const std::string& shape_id, Shape* proto_)
	{
		return prototypes_.insert(std::make_pair(shape_id, boost::shared_ptr<Shape>(proto_))).second;
	}

	bool unregister_shape(const std::string& shape_id)
	{
		return prototypes_.erase(shape_id) != 0;
	}
};

typedef GenericSingleton::SingletonHolder<Factory> ShapeFactory;

}

#endif