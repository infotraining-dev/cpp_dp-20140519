#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

#include <iostream>

class Singleton
{
public:
    static Singleton& instance()
    {
        if (!instance_)
        {
            instance_ = new Singleton();
        }

        return *instance_;
    }

	void do_something();

private:
    static Singleton* instance_;  // uniqueInstance
	
	Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
	{ 
		std::cout << "Constructor of singleton" << std::endl; 
	} 

	Singleton(const Singleton&);
	Singleton& operator=(const Singleton&);

	~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
	{ 
		std::cout << "Singleton has been destroyed!" << std::endl;
	} 
};

Singleton* Singleton::instance_ = NULL;

void Singleton::do_something()
{
	std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
}

#endif /*SINGLETON_HPP_*/
