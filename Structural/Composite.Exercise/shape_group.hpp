#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <vector>
#include <functional>
#include <algorithm>

namespace Drawing
{

// TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
class ShapeGroup : public Shape
{
    std::vector<Shape*> shapes_;
public:
    ShapeGroup() {}

    ShapeGroup(const ShapeGroup& sc)
    {
        std::transform(sc.shapes_.begin(), sc.shapes_.end(),
                       std::back_inserter(shapes_),
                       std::mem_fn(&Shape::clone));
    }

    ~ShapeGroup()
    {
        for(auto shape : shapes_)
            delete shape;
    }

    void add_shape(Shape* shape)
    {
        shapes_.push_back(shape);
    }

    void draw() const
    {
        std::for_each(shapes_.begin(), shapes_.end(), std::mem_fn(&Shape::draw));
    }

    ShapeGroup* clone() const
    {
        return new ShapeGroup(*this);
    }

    void move(int dx, int dy)
    {
        std::for_each(shapes_.begin(), shapes_.end(),
                      std::bind(&Shape::move, std::placeholders::_1, dx, dy));
    }

    void read(std::istream &in)
    {
        int count;

        in >> count;

        for(int i = 0; i < count; ++i)
        {
            std::string type_id;
            in >> type_id;

            Shape* shape = ShapeFactory::instance().create(type_id);
            shape->read(in);

            add_shape(shape);
        }
    }

    void write(std::ostream &out)
    {
        for(auto shape : shapes_)
            shape->write(out);
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
