#ifndef COMPOSITE_HPP_
#define COMPOSITE_HPP_

#include <iostream>
#include <string>
#include <list>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

// "Component" 
class Component
{
protected:
	std::string name_;
public:
	Component(const std::string& name) :
		name_(name)
	{
	}

	virtual void display(int depth) = 0;
	
	virtual ~Component() 
	{
		std::cout << "~Component()\n";
	}
};

// "Composite" 
class Composite : public Component
{
private:
	std::list<boost::shared_ptr<Component> > children;
public:
	Composite(const std::string& name) :
		Component(name)
	{
	}

	void add(boost::shared_ptr<Component> c)
	{
		children.push_back(c);
	}

	void remove(boost::shared_ptr<Component> c)
	{
		children.remove(c);
	}

	void display(int depth)
	{
		std::cout << std::string(depth, '-') << name_ << std::endl;
        std::for_each(children.begin(), children.end(),
                      boost::bind(&Component::display, _1, depth+2));
	}
	
	~Composite()
	{
	}
};

// "Leaf"
class Leaf : public Component
{
public:
	Leaf(const std::string& name) :
		Component(name)
	{
	}

	void display(int depth)
	{
		std::cout << std::string(depth, '-') << name_ << std::endl;
	}
};

#endif /*COMPOSITE_HPP_*/
