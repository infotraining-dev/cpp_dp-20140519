#include "paragraph.hpp"

#include <iostream>

using namespace std;

class Service
{
public:
    virtual void run()
    {
        std::cout << "Service.run()" << std::endl;
    }

    virtual ~Service()
    {
    }
};

class ServiceWithLog : public Service
{
    Service& srv_;
public:
    ServiceWithLog(Service& srv) : srv_(srv)
    {
    }

    void run()
    {
        std::cout << "Logging\n";
        srv_.run();
    }
};

void client(Service& srv)
{
    srv.run();
}

int main()
{
    Service srv;
    ServiceWithLog srv_with_log(srv);
    client(srv_with_log);

	// normal paragraph
	Paragraph* p = new Paragraph("Test of paragraph");
	cout << "Normal paragraph: " << p->getHTML() << endl;
	delete p;

	// bolded paragraph
	Paragraph* bold = new BoldParagraph(new Paragraph("Test of bold paragraph"));
	cout << "Bolded paragraph: " << bold->getHTML() << endl;
	delete bold;

	// bold and italic
	Paragraph* bold_and_italic = new ItalicParagraph(new BoldParagraph(new Paragraph("Test of italic and bold paragraph")));
	cout << "Bold and italic: " << bold_and_italic->getHTML() << endl;
	delete bold_and_italic;
}
